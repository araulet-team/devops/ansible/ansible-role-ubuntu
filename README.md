# Ansible Role Ubuntu

Role for configuring Ubuntu.

## Requirements
This role requires Ansible 2.6 or higher, and platform requirements are listed in the metadata file.

## Installation

add this in **requirements.yml**

```shell
- src: git+https://gitlab.com/araulet-team/devops/ansible/ansible-role-ubuntu.git
  name: araulet.ubuntu
```

## Role Variables
The variables that can be passed to this role and a brief description about them are as follows:

* **themes.yml**: install pop theme

* **groups.yml**: add users to groups

    ```yaml
  user_to_groups:
    - name: vagrant
      groups:
        - docker
        - sudo
    ```

* **packages.yml**: load new packages

    ```yaml
  apt:
    update_cache: yes
    cache_valid_time: 3600
    upgrade: no
    packages:
      - curl
      - wget
      - whois
    ```

* **system**: setup `locales`, `watch files` etc.

    ```yaml
  system:
    language: en_US
    ```

* **reboot**: helper scritp to reboot machine

## Example Playbook

```yaml
  roles:
    - role: epok75.ubuntu
      become: true
      
      vars:
        debug:
          enabled: false

        cleanup:
          enabled: false

        group:
          enabled: false

        users:
          enabled: false

        packages:
          enabled: false

        dconf:
          enabled: false

        themes:
          enabled: false

        kernel:
          enabled: false

        reboot:
          enabled: false

        apt:
          update_cache: yes
          cache_valid_time: 3600
          upgrade: no
          packages:
            - curl
            - wget
            - whois

        user_to_groups:
          - name: test
            groups:
              - sudo

        system:
          language: en_US
```

## Dependencies

None

## Licence
MIT

## Author Information
Arnaud Raulet